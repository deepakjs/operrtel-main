export const environment = {
  production: true,
  api: {
      // root: 'http://69.18.218.118:8085',
      // server: 'http://69.18.218.118:8085/api/v1'
      root: 'https://qaapi.operrtel.com',
      server: 'https://qaapi.operrtel.com/api/v1',
      billing: {
          payment: '/billing/api/v3/payment',
          paymentProfile: '/billing/api/v3/payment-profile',
      },
  },
    payment_gateway : {
        server: 'https://payment-gateway.operr.com/',
        app_key: '56f7081add9e4cf0bc8728f47037c749'
    },
};
