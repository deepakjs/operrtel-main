// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    api: {
        root: 'https://prodapi.operrtel.com',
        server: 'https://prodapi.operrtel.com/api/v1',
        billing: {
            payment: '/billing/api/v3/payment',
            paymentProfile: '/billing/api/v3/payment-profile',
          },
    },
    payment_gateway : {
        server: 'https://payment-gateway.operr.com/',
        app_key: '11ac599dfa164ba7b707aaa01176438'
    },
    env: 'dev'
};
