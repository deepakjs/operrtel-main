import {Component, OnInit, ViewChild} from '@angular/core';
import {SerenityServiceService} from '@app/auth/service/serenity-service.service';
import {FormControl, FormGroup} from '@angular/forms';
import {NodeModel} from '@app/_model/node-model';
import {Table} from 'primeng';
import * as uuid from 'uuid';
import * as _ from 'lodash';

@Component({
    selector: 'app-node-page',
    templateUrl: './node-page.component.html',
    styleUrls: ['./node-page.component.css']
})
export class NodePageComponent implements OnInit {

    constructor(private serenityService: SerenityServiceService) {
    }

    nameOfButton = 'Edit';
    @ViewChild('table') public dataTable: Table;
    messageFlag: string;
    name = '';
    username = '';
    password = '';
    privateKey = '';
    url = '';
    nodeType = 1;
    ipAddress = '';
    selectedValues: any[] = [];
    cols: any[] = [
        {field: 'name', label: 'Name'},
        {field: 'ipAddress', label: 'IP Address'},
        {field: 'url', label: 'URL'},
        {field: 'nodeType', label: 'Node Type'},
        {field: 'username', label: 'username'},
        {field: 'password', label: 'password'},
        {field: 'privateKey', label: 'Private Key'},
        {field: 'edit', label: 'Action edit'}
    ];

    mapValue = {
        1: 'SIP',
        2: 'WebRTC'
    };

    nodeModelFormGroup: FormGroup;
    nodeModels: Array<NodeModel> = [];

    ngOnInit(): void {
        this.initData();
    }

    initData() {
        this.nodeModelFormGroup = new FormGroup({});
    }

    onRowEditInit(rowData: NodeModel) {
        if (rowData.nameOfButton === 'Edit') {
            rowData.nameOfButton = 'Save';
            this.initRowEdit(rowData);
        } else {
            this.update(rowData);
            rowData.nameOfButton = 'Edit';
        }
    }

    private initRowEdit(rowData: NodeModel) {
        const formGroupOfRow = new FormGroup({
            id: new FormControl(rowData.id),
            name: new FormControl(rowData.name),
            ipAddress: new FormControl(rowData.ipAddress),
            url: new FormControl(rowData.url),
            nodeType: new FormControl(rowData.nodeType),
            username: new FormControl(rowData.username),
            password: new FormControl(rowData.password),
            privateKey: new FormControl(rowData.privateKey)
        });
        this.nodeModelFormGroup.setControl(rowData.dataKey, formGroupOfRow);
        this.dataTable.initRowEdit(rowData);
    }

    checkConnection() {
        this.serenityService.checkConnection(this.ipAddress, this.name, this.nodeType, this.password, this.privateKey, this.url,
            this.username).subscribe(next => {
                if (next.status === 'false') {
                    this.messageFlag = next.message;
                } else {
                    this.messageFlag = 'connect successfully';
                }
            },
            error => {
                this.messageFlag = 'not connect successfully';
            });
    }

    removeAlert() {
        this.messageFlag = null;
    }

    search() {
        this.serenityService.searchNode(0, this.ipAddress, this.name, this.nodeType, this.password, this.privateKey, this.url,
            this.username)
            .subscribe(
                next => {
                    this.nodeModels = next;
                    this.nodeModels.forEach(d => {
                        d.dataKey = uuid.v4();
                        d.nameOfButton = 'Edit';
                        this.nodeModelFormGroup.addControl(d.dataKey, new FormGroup({}));
                    });
                    if (this.nodeModels.length === 0) {
                        this.messageFlag = 'no record found!!!';
                    }
                }
            );
    }

    addConfirm() {
        if (confirm('Are you sure to ADD this node?')) {
            this.add();
        }
    }

    add() {
        this.serenityService.createNode(this.ipAddress, this.name, this.nodeType, this.password, this.privateKey, this.url, this.username)
            .subscribe(next => {
                this.search();
                this.name = '';
                this.username = '';
                this.password = '';
                this.privateKey = '';
                this.url = '';
                this.nodeType = undefined;
                this.ipAddress = '';
                this.messageFlag = 'add node successfully';
            });
    }

    update(rowData: NodeModel) {
        // Get data from formControl to sent to server
        const dataForUpdate = [];
        this.nodeModels.forEach((d: any) => {
            const formGroup: any = this.nodeModelFormGroup.controls[d.dataKey];
            const formControls = formGroup.controls;
            if (!_.isEmpty(formControls) && rowData.id === formControls.id.value) {
                d.name = formControls.name.value;
                d.username = formControls.username.value;
                d.password = formControls.password.value;
                d.url = formControls.url.value;
                d.nodeType = Number(formControls.nodeType.value);
                d.ipAddress = formControls.ipAddress.value;
                d.privateKey = formControls.privateKey.value;
                dataForUpdate.push(d);
            }
        });
        this.serenityService.updateNode(dataForUpdate).subscribe(next => {
                if (next.status === 'false') {
                    this.messageFlag = next.message;
                } else {
                    this.messageFlag = 'update successfully';
                    this.search();
                }
            },
            error => {
                this.messageFlag = 'update fail';
            });
    }

    editConfirm(rowData: NodeModel) {
        if (confirm('Are you sure to edit this row?')) {
            this.onRowEditInit(rowData);
        }
    }

}
