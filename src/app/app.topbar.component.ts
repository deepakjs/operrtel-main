import {Component} from '@angular/core';
import {AppMainComponent} from './app.main.component';
import {AuthService} from '@app/auth/service/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'
})
export class AppTopBarComponent {
    username: string = '';
    
    constructor(public app: AppMainComponent, private authService: AuthService) {
        this.username = authService.getUsername();
    }
    logOut() {
        this.authService.logout();
    }
}
