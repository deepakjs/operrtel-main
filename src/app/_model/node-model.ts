export class NodeModel {
    dataKey: string;
    id: any;
    name: string;
    username: string;
    password: string;
    privateKey: string;
    url: string;
    nodeType: number;
    ipAddress: string;
    nameOfButton = 'Edit';
}
