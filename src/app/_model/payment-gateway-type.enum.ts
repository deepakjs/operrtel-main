export enum PaymentGatewayType {
    AUTHORIZE_NET = 0,
    BRAINTREE = 1,
    STRIPE = 2,
    PAYPAL = 3,
    CASH = 3,
}
