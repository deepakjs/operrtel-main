export enum PaymentCardType {
    VISA = 1,
    MASTER_CARD = 2,
    DISCOVER = 3,
    AMERICAN_EXPRESS = 4,
    PAYPAL = 5,
}
