export class User {
    id: number;
    username: string;
    password: string;
    accessLevel: string;
    email: string;
    contactNumber: string;
    description: string;
}
