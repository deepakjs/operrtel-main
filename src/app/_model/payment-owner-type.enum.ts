export enum PaymentOwnerType {
    BASE = 0,
    COMPANY = 1,
    DRIVER = 2,
    CUSTOMER = 3,
}
