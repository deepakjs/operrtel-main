import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {AppRoutes} from './app.routes';
import { TableModule } from 'primeng/table';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import { AccordionModule } from 'primeng/accordion';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ButtonModule } from 'primeng/button';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { SelectButtonModule } from 'primeng/selectbutton';
import { SlideMenuModule } from 'primeng/slidemenu';
import { SliderModule } from 'primeng/slider';
import { SpinnerModule } from 'primeng/spinner';
import { ToolbarModule } from 'primeng/toolbar';
import { TooltipModule } from 'primeng/tooltip';
import {TabViewModule} from 'primeng/tabview';
import {PanelModule} from 'primeng/panel';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { MenuModule } from 'primeng/menu';
import {ToastModule} from 'primeng/toast';
import { VirtualScrollerModule } from 'primeng/virtualscroller';
import {ConfirmationService, MessageService} from 'primeng/api';

import { AppComponent } from './app.component';
import { AppMainComponent } from './app.main.component';
import { AppConfigComponent } from './app.config.component';
import { AppNotfoundComponent } from './pages/app.notfound.component';
import { AppErrorComponent } from './pages/app.error.component';
import { AppAccessdeniedComponent } from './pages/app.accessdenied.component';
import { AppLoginComponent } from './pages/app.login.component';
import {AppMenuComponent} from './app.menu.component';
import { AppMenuitemComponent } from './app.menuitem.component';
import {AppBreadcrumbComponent} from './app.breadcrumb.component';
import {AppTopBarComponent} from './app.topbar.component';
import {AppFooterComponent} from './app.footer.component';
import {DocumentationComponent} from './demo/view/documentation.component';
import {CarService} from './demo/service/carservice';
import {CountryService} from './demo/service/countryservice';
import {EventService} from './demo/service/eventservice';
import {NodeService} from './demo/service/nodeservice';

import {BreadcrumbService} from './breadcrumb.service';
import {MenuService} from './app.menu.service';
import { LoginComponent } from './auth/login/login.component';
import {AuthService} from '@app/auth/service/auth.service';
import {AddHttpHeaderInterceptor} from '@app/core/http-interceptor/custom.header.http';
import { NodePageComponent } from './pages/node-page/node-page.component';
import { UserComponent } from './demo/view/user/user.component';

import { UserService } from './auth/service/user.service';
import { CustomerSearchService } from './auth/service/customer-search.service';
import { NotificationService } from './auth/service/notification.service';
import { AesUtilService } from './auth/service/aes-util.service';
import { PaymentProfileService } from './auth/service/payment-profile.service';
import { TariffService } from './auth/service/tariff.service';
import { DidConnectService } from './auth/service/did-connect.service';
import { SmsSearchService } from './auth/service/sms-search.service';
import { DashboardComponent } from './demo/view/dashboard.component';
import { CdrsComponent } from './demo/view/cdrs/cdrs.component';
import { HistoryComponent } from './demo/view/history/history.component';
import { PaymentComponent } from './demo/view/payment/payment.component';
import { ForgotLoginComponent } from './auth/forgot-login/forgot-login.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutes,
        HttpClientModule,
        BrowserAnimationsModule,
        AccordionModule,
        TableModule,
        AutoCompleteModule,
        BreadcrumbModule,
        ButtonModule,
        ScrollPanelModule,
        SelectButtonModule,
        SlideMenuModule,
        SliderModule,
        SpinnerModule,
        ToolbarModule,
        TooltipModule,
        VirtualScrollerModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        ConfirmDialogModule,
        TabViewModule,
        PanelModule,
        ToastModule,
        CheckboxModule,
        DropdownModule,
        MenuModule
    ],
    declarations: [
        DashboardComponent,
        AppComponent,
        AppMainComponent,
        AppMenuComponent,
        AppMenuitemComponent,
        AppConfigComponent,
        AppBreadcrumbComponent,
        AppTopBarComponent,
        AppFooterComponent,
        DocumentationComponent,
        AppNotfoundComponent,
        AppErrorComponent,
        AppLoginComponent,
        LoginComponent,
        NodePageComponent,
        UserComponent,
        CdrsComponent,
        AppAccessdeniedComponent,
        HistoryComponent,
        PaymentComponent,
        ForgotLoginComponent
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: AddHttpHeaderInterceptor, multi: true},
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        CarService, CountryService, EventService, NodeService, BreadcrumbService, MenuService, AuthService, UserService, ConfirmationService,
        MessageService,
        CustomerSearchService,
        NotificationService,
        AesUtilService,
        TariffService,
        PaymentProfileService,
        DidConnectService,
        SmsSearchService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
