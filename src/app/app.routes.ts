import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import { UserComponent } from './demo/view/user/user.component';
import {DocumentationComponent} from './demo/view/documentation.component';
import {AppMainComponent} from './app.main.component';
import {AppNotfoundComponent} from './pages/app.notfound.component';
import {AppErrorComponent} from './pages/app.error.component';
import {AppAccessdeniedComponent} from './pages/app.accessdenied.component';
import {AppLoginComponent} from './pages/app.login.component';
import {LoginComponent} from './auth/login/login.component';
import {RouteGuardService} from '@app/auth/service/route-guard.service';
import {NodePageComponent} from '@app/pages/node-page/node-page.component';
import { DashboardComponent } from './demo/view/dashboard.component';
import { CdrsComponent } from './demo/view/cdrs/cdrs.component';
import { HistoryComponent } from './demo/view/history/history.component';
import { PaymentComponent } from './demo/view/payment/payment.component';
import { ForgotLoginComponent } from './auth/forgot-login/forgot-login.component';

export const routes: Routes = [
    {path: '', component: LoginComponent},
    {path: 'login', component: LoginComponent},
    {path: 'forgot-password', component: ForgotLoginComponent},
    {
        path: '', component: AppMainComponent,
        children: [
            {path: 'dashboard', component: DashboardComponent},
            {path: 'cdrs', component: CdrsComponent},
            {path: 'payment', component: PaymentComponent},
            {path: 'history', component: HistoryComponent},
            {path: 'documentation', component: DocumentationComponent},
            {path: 'user-list', component: UserComponent}
        ]
        , canActivate: [RouteGuardService]
    },
    
    {path: 'error', component: AppErrorComponent},
    {path: 'accessdenied', component: AppAccessdeniedComponent},
    {path: 'notfound', component: AppNotfoundComponent},
    {path: 'login', component: AppLoginComponent},
    {path: '**', redirectTo: '/notfound'},

];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'});
