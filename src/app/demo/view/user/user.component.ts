import { Component, OnInit } from '@angular/core';
import { UserService } from '@app/auth/service/user.service';
import {ConfirmationService, MessageService} from 'primeng/api';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  data = [];
  constructor(
    private userService: UserService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService) {

  }

  ngOnInit(): void {
    this.getUser();
  }
  getUser() {
    this.userService.getUserList().subscribe(
      data => {
          this.data = data;             
      }
  );
  }
  deleteUser(id) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the user',
      accept: () => {
        this.userService.deleteUser(id).subscribe(res => {
          this.messageService.add({key: 'tc', severity:'success', summary: 'Success', detail: 'User Delete Successfully'});
          this.getUser();
        })
      }
    });
    
  }
}
