import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';







import * as _moment from 'moment';
import * as _ from 'lodash';
import { ConfirmationService} from 'primeng';

import { noWhitespaceFirstAndLastValidator } from '@app/validator/no.whitespace.first.and.last.validator';
import { numericValidator } from '@app/validator';
import { PaymentCardType } from '@app/_model/payment-card-type.enum';
import { PaymentGatewayType } from '@app/_model/payment-gateway-type.enum';
import { PaymentOwnerType } from '@app/_model/payment-owner-type.enum';
import { NotificationService, NotificationType } from '@app/auth/service/notification.service';
import { AesUtilService } from '@app/auth/service/aes-util.service';
import { PaymentProfileService } from '@app/auth/service/payment-profile.service';
import { AuthService } from '@app/auth/service/auth.service';
import { CustomerService } from '@app/auth/service/customer.service';


declare let paypal: any;

@Component({
  selector: 'cus-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css', './payment.component.scss']
})
export class PaymentComponent implements OnInit, AfterViewInit, OnDestroy {
  CUSTOMER_OWNER_TYPE = PaymentOwnerType[PaymentOwnerType.CUSTOMER];
  GATEWAY_TYPE = PaymentGatewayType[PaymentGatewayType.AUTHORIZE_NET];
  PAYMENT_CARD_TYPE = {
    VISA: PaymentCardType[PaymentCardType.VISA],
    MASTER_CARD: PaymentCardType[PaymentCardType.MASTER_CARD],
    AMERICAN_EXPRESS: PaymentCardType[PaymentCardType.AMERICAN_EXPRESS],
    PAYPAL: PaymentCardType[PaymentCardType.PAYPAL],
  };
  CARD_MAPPING = {
    'Visa': PaymentCardType.VISA,
    'MasterCard': PaymentCardType.MASTER_CARD,
    'Discover': PaymentCardType.DISCOVER,
    'AmericanExpress': PaymentCardType.AMERICAN_EXPRESS,
    'Paypal': PaymentCardType.PAYPAL
  };
  selectedTab = 0;
  customers = [];
  selectCustomer = null;
  public formGroup: FormGroup;
  months = [
    {label: '01', value: '01'},
    {label: '02', value: '02'},
    {label: '03', value: '03'},
    {label: '04', value: '04'},
    {label: '05', value: '05'},
    {label: '06', value: '06'},
    {label: '07', value: '07'},
    {label: '08', value: '08'},
    {label: '09', value: '09'},
    {label: '10', value: '10'},
    {label: '11', value: '11'},
    {label: '12', value: '12'},
  ];
  cards = [];
  paymentCardType = null;
  creditCard = null;
  paymentCardTypes = [
    {value: this.PAYMENT_CARD_TYPE.VISA, label: this.PAYMENT_CARD_TYPE.VISA},
    {value: this.PAYMENT_CARD_TYPE.MASTER_CARD, label: this.PAYMENT_CARD_TYPE.MASTER_CARD},
    {value: this.PAYMENT_CARD_TYPE.AMERICAN_EXPRESS, label: this.PAYMENT_CARD_TYPE.AMERICAN_EXPRESS}
  ];

  promoCode: any = '';

  messageMonthError: string = '';
  messageYearError: string = '';
  messageCvvError: string = '';
  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private notificationService: NotificationService,
              private aesUtilService: AesUtilService,
              private paymentProfileService: PaymentProfileService,
              private customerService: CustomerService,
              private confirmationService: ConfirmationService
  ) {
    this.buildForm();
    
    window['paymentComponentContext'] = this;
  }

  ngOnInit(): void {
    this.getCustomers();
    
  }
  getCustomers() {
    this.customerService.getCustomers().subscribe(customerList => {
      this.customers = customerList
      this.selectCustomer = this.customers[0].customerId;
      this.getPaymentProfile();
    });
  }
  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    delete window['paymentComponentContext'];
  }

  getPaymentProfile() {
    this.paymentProfileService.getAllCreditCards(this.selectCustomer).subscribe(
      rp => {
        if(rp.data !== 'Not Found') {
          this.cards = (rp.data as Array<any>).filter(card => card.cardType != this.PAYMENT_CARD_TYPE.PAYPAL);
        }
      }
    )
  }

  onSelectCustomer(ev) {
    this.selectCustomer = ev.target.value;
    this.getPaymentProfile();
  }
  deletePaymentMethod(profileId, paymentProfileId, cardType) {
    const ctype = PaymentCardType[this.CARD_MAPPING[cardType]];
    this.paymentProfileService.deletePaymentProfileMethod(profileId, paymentProfileId, ctype).subscribe(() => {
      this.notificationService.open({
        type: NotificationType.SUCCESS,
        body: 'The card deleted successfully'
      });
      this.getPaymentProfile();
    })
  }

  confirmDeleteCard(cardId){
    this.confirmationService.confirm({
        message: 'Are you sure you want to delete card',
        accept: () => {
        this.deletePaymentCard(cardId);
      },
      reject: () => {}
    });
  }

  deletePaymentCard(cardId) {
    const customer = this.selectCustomer;
    this.paymentProfileService.deletePaymentCard(cardId, customer).subscribe(rs => {
      if(rs && rs.status === 200){
        this.notificationService.open({
          type: NotificationType.SUCCESS,
          body: 'The card deleted successfully'
        });
        this.getPaymentProfile();
      }else{
        let bodyMsg = rs.message;
        this.notificationService.open({
          type: NotificationType.ERROR,
          body: bodyMsg
        });
      }
    },
    error => {
      console.log(error);
      let bodyMsg = error.error.error;
      this.notificationService.open({
        type: NotificationType.ERROR,
        body: bodyMsg
      });
    })
  }

  getCreditCard() {
    const customer = this.selectCustomer;
    this.paymentProfileService.getCreditCards(customer).subscribe(rp => {
      if (rp.length) {
        this.creditCard = rp[0];
      }
    });
  }

  tabChanged(e: any) {
    this.selectedTab = e.index;
  }

  cancel() {
    this.goToTab(0);
  }

  goToTab = (tabIndex: number) => {
    this.selectedTab = tabIndex;
    if (tabIndex === 0) {
      this.getPaymentProfile();
    }
  }

  addPaymentMethod() {
    console.log("add payment method", this.formGroup.valid);
    if(this.formGroup.valid){
      const formValues: any = this.formGroup.getRawValue();
      const cardDataStr = `${formValues.cardName};${formValues.cardNumber};${formValues.cvcNumber};${formValues.year};${formValues.month};${formValues.zipcode};${formValues.address};${formValues.city ? formValues.city : ''};${formValues.state ? formValues.state : ''}`;
      const cardType = this.formGroup.get('paymentCardType').value;
      this.paymentProfileService.getPaymentKeyStore().subscribe(
        rp => {
          const encryptData = this.aesUtilService.encryptDataWithKey(cardDataStr.trim(), rp.data.keyStore);
          console.log(encryptData);
          this.createPaymentProfile(formValues, encryptData, cardType, rp.data.id);
        },err => {
          this.notificationService.open({
            type: NotificationType.ERROR,
            body: 'Get key store fail.'
          });
        }
      )
    }
  }

  createPaymentProfile = (formValue, encryptData, cardType, keyId) => {
    const customer = this.selectCustomer;
    
    const payloadMethod = {
      userType: "CUSTOMER",
      userId: customer,
      email: customer.email || "sample@mail.com",
      encryptDataCard: encryptData,
      firstName: formValue.cardName,
      lastName: formValue.lastName,
      gatewayType: "STRIPE",
      paymentCardType: cardType,
      phoneNumber: customer.phone || "1234567890"
    };
    this.paymentProfileService.addCardPaymentProfile(payloadMethod, keyId).subscribe(
      rp => {
        if(rp.status === 801){
          this.notificationService.open({
            type: NotificationType.ERROR,
            body: rp.message
          });
        }else{
          this.addToCustomerPayment(rp.data);
        }
      },
      err => {
        let bodyMsg = err.error.message;
        if (err.error.message === 'Can\'t create payment profile with that information') {
          bodyMsg = 'Credit card number length does not match card type';
        }
        this.notificationService.open({
          type: NotificationType.ERROR,
          body: bodyMsg
        });
      }
    )
  }
  addToCustomerPayment(data) {
    const customer = this.selectCustomer;
    const cardObj = {
      "customerId": customer,
      "isDefault": 1,
      "paymentCardId": data.cardInfo.id,
      "paymentUserId": data.user.id,
      "status": data.user.status
    }
    this.paymentProfileService.addPaymentDataToCustomer(cardObj, customer).subscribe(data => {
      console.log(data);
      this.notificationService.open({
        type: NotificationType.SUCCESS,
        body: 'Add Payment Method Successfully'
      });
      this.goToTab(0);
    })
  }
  buildForm() {
    this.formGroup = this.fb.group({
      cardNumber: [null, [Validators.required, noWhitespaceFirstAndLastValidator(), numericValidator(), Validators.maxLength(16), Validators.minLength(16)]],
      cvcNumber: [null, [noWhitespaceFirstAndLastValidator(), numericValidator(), Validators.maxLength(3)]],
      month: [null, [Validators.required]],
      paymentCardType: [null, [Validators.required]],
      year: [null, [Validators.required, noWhitespaceFirstAndLastValidator(), numericValidator(), Validators.maxLength(4), Validators.minLength(4)]],
      cardName: [null,[Validators.required, noWhitespaceFirstAndLastValidator()]],
      lastName: [null,[Validators.required, noWhitespaceFirstAndLastValidator()]],
      address: [null,[Validators.required, noWhitespaceFirstAndLastValidator()]],
      city: [null,[Validators.required, noWhitespaceFirstAndLastValidator()]],
      country: [null,[Validators.required, noWhitespaceFirstAndLastValidator()]],
      zipcode: [null,[Validators.required, noWhitespaceFirstAndLastValidator(), numericValidator(), Validators.maxLength(5), Validators.minLength(5)]],
      state: [null,[Validators.required, noWhitespaceFirstAndLastValidator()]],
      securelyStoreCard: []
    });
  }

  validateCVV(event) {
    this.messageCvvError = '';
    const valueType = this.formGroup.get('paymentCardType').value;
    const cvv = this.formGroup.get('cvcNumber').value;
    this.messageCvvError = '';
    if (valueType === PaymentCardType[PaymentCardType.AMERICAN_EXPRESS]) {
      if (cvv && cvv.toString().length !== 4) {
        this.messageCvvError = 'The CVV field must me 4 digits long';
      }
    } else {
      if (cvv && cvv.toString().length !== 3) {
        this.messageCvvError = 'The CVV field must me 3 digits long';
      }
    }
  }

  validateYear(event) {
    const momentYear = _moment();
    const valueyear = this.formGroup.get('year').value;
    this.messageYearError = '';
    if (!valueyear) {
      this.messageYearError = 'The year field is required!';
    }
    if (valueyear < momentYear.year()) {
      this.messageYearError = 'The year should be greater than current year';
    }
  }

  validateMonth(event) {
    this.messageMonthError = '';
    const valueyear = this.formGroup.get('year').value;
    if (valueyear) {
      const momentYear = _moment();
      if (valueyear < momentYear.year()) {
        this.messageMonthError = 'The year should be greater than current year';
      }

      const valueMonth = this.formGroup.get('month').value;
      if (valueMonth * 1 <= momentYear.month() + 1 && valueyear <= momentYear.year()) {
        this.messageMonthError = 'The month should be greater than current month';
        this.formGroup.get('month').setValue(null);
      }
    }
  }

  makeDefaultPaymentCard(cardId) {
    this.paymentProfileService.makeDefaultCard(cardId, this.selectCustomer).subscribe(rs => {
        if(rs && rs.status === 200){
          this.notificationService.open({
            type: NotificationType.SUCCESS,
            body: 'Update successfully'
          });
          this.getPaymentProfile();
        }else{
          let bodyMsg = rs.message;
          this.notificationService.open({
            type: NotificationType.ERROR,
            body: bodyMsg
          });
        }
      },
      error => {
        console.log(error);
        let bodyMsg = error.error.error;
        this.notificationService.open({
          type: NotificationType.ERROR,
          body: bodyMsg
        });
      })
  }

}
