import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { interval, Subscription } from 'rxjs';
import { environment } from 'environments/environment';
import { HistoryService } from '@app/auth/service/history.service';

@Component({
  selector: 'history-connect',
  styles: [`
        /* Table */
        .ui-table.ui-table-cars .ui-table-caption.ui-widget-header {
            border: 0 none;
            padding: 12px;
            text-align: left;
            font-size: 20px;
            font-weight: normal;
        }

        .ui-table .ui-table-globalfilter-container {
            position: relative;
            top: -4px;
        }

        .ui-column-filter {
            margin-top: 1em;
        }

        .ui-column-filter .ui-multiselect-label {
            font-weight: 500;
        }

        .ui-table.ui-table-cars .ui-table-thead > tr > th {
            border: 0 none;
            text-align: left;
        }

        .ui-table-globalfilter-container {
            float: right;
            display: inline;
        }

        .ui-table.ui-table-cars .ui-table-tbody > tr > td {
            border: 0 none;
        }

        .ui-table.ui-table-cars .ui-table-tbody .ui-column-title {
            font-size: 16px;
        }

        .ui-table.ui-table-cars .ui-paginator {
            border: 0 none;
            padding: 1em;
        }
    `],
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  
  
  @ViewChildren('messages') messages: QueryList<any>;
  @ViewChild('content') content: ElementRef;

  myDidNumber: string;

  history: any[];

  cols: { field: string; header: string; }[];
  constructor(
    private historyServices: HistoryService
  ) { 
    this.cols = [
      { field: 'from', header: 'from' },
      { field: 'to', header: 'to' },
      { field: 'body', header: 'Body' },
      { field: 'createDatetime', header: 'createDatetime' },
      { field: 'type', header: 'type' },
    ];
  }

  ngAfterViewInit() {
  }
  
  
  ngOnInit(): void {
    // this.getCustomers()
    // this.getAccountsList();
    this.myDidNumber = window.localStorage.getItem('userMobileInfo');
    
    this.getHistory();
  }


  getHistory() {
    this.historyServices.getHistory(this.myDidNumber).subscribe(res => {
      this.history = res.data;
    })
  }
  
  
}
