import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { interval, Subscription } from 'rxjs';
import { environment } from 'environments/environment';
import { CdrsSearchService } from '@app/auth/service/cdrs.service';

@Component({
  selector: 'cdrs-connect',
  styles: [`
        /* Table */
        .ui-table.ui-table-cars .ui-table-caption.ui-widget-header {
            border: 0 none;
            padding: 12px;
            text-align: left;
            font-size: 20px;
            font-weight: normal;
        }

        .ui-table .ui-table-globalfilter-container {
            position: relative;
            top: -4px;
        }

        .ui-column-filter {
            margin-top: 1em;
        }

        .ui-column-filter .ui-multiselect-label {
            font-weight: 500;
        }

        .ui-table.ui-table-cars .ui-table-thead > tr > th {
            border: 0 none;
            text-align: left;
        }

        .ui-table-globalfilter-container {
            float: right;
            display: inline;
        }

        .ui-table.ui-table-cars .ui-table-tbody > tr > td {
            border: 0 none;
        }

        .ui-table.ui-table-cars .ui-table-tbody .ui-column-title {
            font-size: 16px;
        }

        .ui-table.ui-table-cars .ui-paginator {
            border: 0 none;
            padding: 1em;
        }
    `],
  templateUrl: './cdrs.component.html',
  styleUrls: ['./cdrs.component.css']
})
export class CdrsComponent implements OnInit {
  
  
  @ViewChildren('messages') messages: QueryList<any>;
  @ViewChild('content') content: ElementRef;

  myDidNumber: string;

  cdrDetail: any;

  didConnectForm = new FormGroup({
    // to: new FormControl('', [Validators.required]),
    from: new FormControl(''),
    body: new FormControl('', [Validators.required]),
    createDatetime: new FormControl('', [Validators.required]),
    type: new FormControl('string'),
    file: new FormControl('')
  });
  cols: { field: string; header: string; }[];
  constructor(
    private cdrsServices: CdrsSearchService
  ) { 
    this.cols = [
      { field: 'billedDuration', header: 'Billed Duration' },
      { field: 'cldIn', header: 'Cld In' },
      { field: 'cliIn', header: 'Cli In' },
      { field: 'connectTime', header: 'Connect Time' },
      { field: 'cost', header: 'Cost' },
      { field: 'result', header: 'Result' },
      { field: 'type', header: 'Type' }
    ];
  }

  ngAfterViewInit() {
  }
  
  
  ngOnInit(): void {
    // this.getCustomers()
    // this.getAccountsList();
    this.myDidNumber = window.localStorage.getItem('userMobileInfo');
    this.didConnectForm.controls['createDatetime'].setValue(new Date());
    this.getCdrs();
  }
  
  getCdrs() {
    this.cdrsServices.getCdrs(this.myDidNumber).subscribe(res => {
      this.cdrDetail = res;
    })
  }

  getHistory() {
    this.cdrsServices.getHistory(this.myDidNumber).subscribe(res => {
      console.log(res)
    })
  }
  
  
}
