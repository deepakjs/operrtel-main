import {Component, OnInit} from '@angular/core';
import {AppMainComponent} from './app.main.component';
import { AuthService } from './auth/service/auth.service';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    model: any[];

    constructor(public app: AppMainComponent, private authService: AuthService) {}

    ngOnInit() {
        const userInst = this.authService.getUserInfo();
        this.model = [
            {label: 'Dashboard', icon: 'message', routerLink: ['/dashboard']},
            {label: 'CDRS', icon: 'grid_on', routerLink: ['/cdrs']},
            {label: 'History', icon: 'menu', routerLink: ['/history']},
            {label: 'Payment', icon: 'menu', routerLink: ['/payment']},
            
            // {
            //     label: 'Customer', icon: 'list', routerLink: ['/customers'],
            //     items: [
            //         {label: 'Customer', icon: 'account_circle', routerLink: ['/customers']},
            //         {label: 'Directory', icon: 'account_circle', routerLink: ['/contacts']},
            //         // {label: 'Accounts', icon: 'input', routerLink: ['/components/forms']},
            //         // {label: 'Data', icon: 'grid_on', routerLink: ['/components/data']},
            //         // {label: 'Panels', icon: 'content_paste', routerLink: ['/components/panels']},
            //         // {label: 'Overlays', icon: 'content_copy', routerLink: ['/components/overlays']},
            //         // {label: 'Menus', icon: 'menu', routerLink: ['/components/menus']},
            //         // {label: 'Messages', icon: 'message', routerLink: ['/components/messages']},
            //         // {label: 'Charts', icon: 'insert_chart', routerLink: ['/components/charts']},
            //         // {label: 'File', icon: 'attach_file', routerLink: ['/components/file']},
            //         // {label: 'Misc', icon: 'toys', routerLink: ['/components/misc']}
            //     ]
            // },
            
            // {
            //     label: 'System Mngmt', icon: 'verified_user', routerLink: ['/pages'],
            //     items: [
            //         {label: 'User', icon: 'account_circle', routerLink: ['/user-list']},
            //         {label: 'Nodes', icon: 'menu', routerLink: ['/components/node'], hideStatus: (userInst.accessLevel !== 'Root') ? true : false},
            //         // {label: 'Landing Page', icon: 'flight_land', url: 'assets/pages/landing.html', target: '_blank'},
            //         // {label: 'Login Page', icon: 'verified_user', routerLink: ['/login'], target: '_blank'},
            //         // {label: 'Error Page', icon: 'error', routerLink: ['/error'], target: '_blank'},
            //         // {label: '404 Page', icon: 'error_outline', routerLink: ['/404'], target: '_blank'},
            //         // {label: 'Access Denied Page', icon: 'security', routerLink: ['/accessdenied'], target: '_blank'}
            //     ]
            // },
            // {
            //     label: 'Reports', icon: 'menu',
            //     items: [
            //         {label: 'Customer CDRs', icon: 'insert_chart', routerLink: ['/components/cdr']},
            //         {label: 'Customer Report', icon: 'attach_file', routerLink: ['/components/file']}
            //     ]
            // },
            // {label: 'Tools', icon: 'build', routerLink: ['/utils']},
            // {label: 'Documents', icon: 'find_in_page', routerLink: ['/documentation']}
        ];
    }

    onMenuClick() {
        this.app.menuClick = true;
    }
}
