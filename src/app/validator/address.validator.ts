import { FormControl } from '@angular/forms';
import * as _ from 'lodash';

export function addressValidator() {
    return function validate(control: FormControl) {
        if (control && control.value) {
          let value = _.toString(control.value);
          const newValue = value.replace(/[^a-zA-Z0-9\s,\-\/]+/g, '');
          if (value !== newValue) {
            return {
              alphanumeric: true
            };
          }
        }
        return null;
    };
}
