import { FormControl } from '@angular/forms';

export function numericValidator() {
    return function validate(control: FormControl) {
        if (control && control.value !== null && control.value !== undefined) {
          // let value = String(control.value);// Convert to string to use replace function
          // value = value.replace(/[^0-9]+/g, '');
          if (isNaN(control.value)) {
            return {
              numeric: true
            };
          }
        }
        return null;
    };
}
