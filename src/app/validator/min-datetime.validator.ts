import { FormControl } from '@angular/forms';
import * as moment from 'moment';

export function minDateTimeValidator(minDateTime: Date, format: string, isEnable: () => boolean) {
    return function validate(control: FormControl) {
      if (control) {
        const value = String(control.value);
        const time = moment(value, format).toDate().getTime();
        if (isEnable() && time < minDateTime.getTime()) {
          return {
            minDateTime: true
          };
        }
      }
      return null;
    };
}
