import { FormControl } from '@angular/forms';

export function noWhitespaceValidator() {
    return function validate(control: FormControl) {
      if (control) {
        const value = control.value;
        if (String(value).split(" ").join("") === "") {
          return {
            required: true
          };
        }
      }
      return null;
    };
}
