import { FormControl } from '@angular/forms';

export function notBlank() {
    return function validate(control: FormControl) {
        if (control && control.value) {
          let isWhitespace = (control.value || '').trim().length === 0;
          let isValid = !isWhitespace;
          return isValid ? null : { 'whitespace': true }
        }
        return null;
    };
}
