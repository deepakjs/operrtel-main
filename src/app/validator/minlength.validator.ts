import {FormControl, Validators} from '@angular/forms';

export function minLengthValidator(length: number) {
    return function validate(control: FormControl) {
      if (control && control.value !== undefined && control.value !== null) {
        if (String(control.value).length < length) {
          return {
            minlength: {
              requiredLength: length
            }
          }
        }
      }
      return null;
    };
}
