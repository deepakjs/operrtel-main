import { FormControl } from '@angular/forms';
import * as _ from 'lodash';
import {Observable} from 'rxjs';

export function alphaNumericValidator() {
    return function validate(control: FormControl) {
        if (!control.value || control.value.length === 0) {
          return null;
        }
        if (control && control.value) {
          let value = _.toString(control.value);
          const newValue = value.replace(/[^a-zA-Z0-9\s,\-]+/g, '');
          if (value !== newValue) {
            return {
              alphanumeric: true
            };
          }
        }
        return null;
    };
}
