import { AbstractControl } from '@angular/forms';
import { ValidatorError } from './constant';

export function UsernameValidator(control: AbstractControl) {
    // if (/^[a-z0-9_]{3,50}$/.test(control.value)) {
    //     return null;
    // }
    if(/^$/.test(control.value)){
        return null;
    }
    if(!(/^[a-z0-9_]*$/.test(control.value))){
        return {[ValidatorError.USERNAME_CHAR]: true};
    }
    else if (!(/^[a-z0-9_]{6,30}$/.test(control.value))){
        return {[ValidatorError.USERNAME]: true};
    }

    else{
        return null;
    }
}
