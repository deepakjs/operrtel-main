import { FormControl } from '@angular/forms';

export function alphabetValidator(acceptString?: string) {
    return function validate(control: FormControl) {
        if (control && control.value) {
          let value = control.value;
          if (acceptString) {
            for (const c of acceptString) {
              value = value.split(c).join('');
            }
          }
          const newValue = value.replace(/[^a-zA-Z\s]+/g, '');
          if (value !== newValue) {
            return {
              alphabet: true
            };
          }
        }
        return null;
    };
}
