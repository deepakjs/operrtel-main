import { FormControl } from '@angular/forms';

export function noWhitespaceFirstAndLastValidator() {
    return function validate(control: FormControl) {
      if (control) {
        const value = String(control.value);
        if (value.startsWith(" ") || value.endsWith(" ")) {
          return {
            whitespace: true
          };
        }
      }
      return null;
    };
}
