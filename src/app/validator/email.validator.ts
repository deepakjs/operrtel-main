import {FormControl, Validators} from '@angular/forms';

export function emailValidator() {
    return function validate(control: FormControl) {
      if (control) {
        if (!control.value || control.value === "") {
          return null;
        }
        const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!pattern.test(control.value)) {
          return {
            email: true
          };
        } else {
          return null;
        }
      }
      return null;
    };
}
