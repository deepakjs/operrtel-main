import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  constructor(private http: HttpClient) { }
  getContacts(username) {
    return this.http.get(`${environment.api.server}/customer-contacts?username=${username}`).pipe(map((response: any) => response));
  }
  getContact(contact) {
    return this.http.get(`${environment.api.server}/customer-contacts?username=19292990008&name=${contact}`);
  }
  saveContact(contact) {
    return this.http.post(`${environment.api.server}/customer-contacts`, contact);
  }
  updateContact(contact) {
      return this.http.put(`${environment.api.server}/customer-contacts`, contact);
  }
  deleteContact(id) {
      return this.http.delete(`${environment.api.server}/customer-contacts/${id}` );
  }
}
