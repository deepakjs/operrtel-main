import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable, Output} from '@angular/core';
import {Router} from '@angular/router';

import * as CryptoJS from 'crypto-js';
import * as _ from 'lodash';
import * as moment from 'moment';
import {CookieService} from 'ngx-cookie-service';
import * as UrlPattern from 'url-pattern';

import {environment} from '../../../environments/environment';
import {AppUrl} from '../../_config/constant/app-url.enum';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthService {
    @Output() medicaidChange = new EventEmitter();
    private userInfo: {
        token?: string, userId?: number, userName?: string
    } = {};
    private loginInfo: any;
    // tslint:disable-next-line: variable-name
    private _isFirstLogin = false;

    private expirationTime = 3600 * 24;

    private SECRET_KEY = '13Xt2sMT';

    private readonly APP_URL_PATTERN: { url: AppUrl; pattern: UrlPattern; }[];

    private readonly INACTIVE_INTERVAL = 600000;

    constructor(private http: HttpClient,
                private router: Router,
                private cookieService: CookieService) {
        this.APP_URL_PATTERN = _.map(AppUrl, (e) => {
            return {url: e, pattern: new UrlPattern(e)};
        });
    }

    isUserLoggedIn() {
        const user = sessionStorage.getItem('operrtel_sys_user');
        return !(user === null);
    }

    private saveUserInfoToEncryptedToken(token: string, userId: number, userName: string) {

        this.userInfo = {token, userId, userName};
        const cipherText = CryptoJS.AES.encrypt(JSON.stringify(this.userInfo), this.SECRET_KEY).toString();
        // Add login info to sessionStorage to support force logout another tab and keep the current tab
        sessionStorage.setItem('operrtel_sys_user', cipherText);
        this.cookieService.set('operrtel_sys_user', cipherText, moment().add(this.expirationTime, 'seconds').toDate());
    }

    getToken() {
        if (sessionStorage.getItem('operrtel_sys_user')) {
            const data = sessionStorage.getItem('operrtel_sys_user');
            const _decrypted = CryptoJS.AES.decrypt(data, this.SECRET_KEY);
            const decrypted = _decrypted.toString(CryptoJS.enc.Utf8);
            return JSON.parse(decrypted).token;
        }
        return null;
    }

    getUsername() {
        if (sessionStorage.getItem('operrtel_sys_user')) {
            const data = sessionStorage.getItem('operrtel_sys_user');
            const _decrypted = CryptoJS.AES.decrypt(data, this.SECRET_KEY);
            const decrypted = _decrypted.toString(CryptoJS.enc.Utf8);
            return JSON.parse(decrypted).userName;
        }
        return null;
    }

    setToken(token: any) {
        if (token) {
            this.userInfo.token = token;
        }
    }

    getLoginInfo() {
        return this.loginInfo || {};
    }
    getUserInfo() {
        const userInfo = window.localStorage.getItem('userInfo');
        return JSON.parse(userInfo);
    }
    setLoginInfo(loginInfo: any) {
        this.loginInfo = loginInfo;
    }

    getUserName() {
        return this.getLoginInfo() && this.getLoginInfo().userName;
    }

    // getLoginType(): LoginType {
    //     return this.userInfo.loginType;
    // }

    getLoginURL() {
        return `${environment.api.server}/login`;
    }

    phoneLogin(phoneNumber: {phoneNumber: string}):Observable<boolean> {
        return this.http.get(`${environment.api.server}/misc/otp?phoneNumber=${phoneNumber.phoneNumber}`).pipe(
            map((response: any) => {
                if (response) {
                    window.localStorage.setItem('userMobileInfo', phoneNumber.phoneNumber);
                    return true;
                } else {
                    return false;
                }
        })
        );;
    }
    phoneLoginOTP(loginValue: {username: string, password: string}):Observable<boolean> {
        const params = {
            username: loginValue.username,
            webPassword: loginValue.password
        };
        return this.http.post(`${environment.api.server}/accounts/authenticate`, params).pipe(
            map((response: any) => {
                console.log(response);
                if (response.status === 'true') {
                    window.localStorage.setItem('userMobileInfo', loginValue.username);
                    return response;
                } else {
                    return response;
                }
        })
        );
    }
    login(loginValue: {
        username: string,
        password: string
    }): Observable<boolean> {
        const URL = this.getLoginURL();

        const params = {
            username: loginValue.username,
            password: CryptoJS.MD5(loginValue.password).toString()
        };

        return this.http.post(URL, params, {observe: 'response'}).pipe(
            map((response: any) => {
                if (response) {
                    this.saveUserInfoToEncryptedToken(this.userInfo.token, response.body.id, response.body.username);
                    window.localStorage.setItem('userInfo', JSON.stringify(response.body));
                    this.setLoginInfo(response);
                    this._isFirstLogin = true;
                    return true;
                } else {
                    return false;
                }
            })
        );
    }

    getLogoutURL() {
        return environment.api.server + '/logout';
    }

    logout(): void {
        sessionStorage.clear();
        window.localStorage.removeItem('userMobileInfo');
        window.localStorage.removeItem('userInfo');
        const URL = this.getLogoutURL();
        this.http.post(URL, {}).subscribe(res => {
            this.redirectLogin();
        }, error => {
            this.redirectLogin();
        });
    }
    
    redirectLogin() {
        this.cookieService.deleteAll();
        sessionStorage.removeItem('operrtel_sys_user');
        this.setLoginInfo({});
        this.userInfo = {};
        this.router.navigate([AppUrl.LOGIN]);
    }
}
