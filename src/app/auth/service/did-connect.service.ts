import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DidConnectService {
  constructor(private http: HttpClient) { }
  
  getDidConnectList() {
    return this.http.get(`${environment.api.server}/did-numbers/assign`).pipe(map((response: any) => response));
  }
}
