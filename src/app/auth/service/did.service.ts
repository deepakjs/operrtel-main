import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DidService {
  constructor(private http: HttpClient) { }
  
  getDidList() {
    return this.http.get(`${environment.api.server}/did-numbers`).pipe(map((response: any) => response));
  }
  getDidListByPool(pool, countryCode?) {
    return this.http.get(`${environment.api.server}/did-numbers?status=${pool}`).pipe(map((response: any) => response));
  }
  getDidListByPoolList(pool, countryCode) {
    return this.http.get(`${environment.api.server}/did-numbers?status=${pool}&areaCode=${countryCode}`).pipe(map((response: any) => response));
  }
  getDidAssignAccounts(id) {
    return this.http.get(`${environment.api.server}/did-numbers/assign?customerId=${id}`).pipe(map((response: any) => response));
  }
  saveDid(did) {
    return this.http.post(`${environment.api.server}/did-numbers`, did).pipe(map((response: any) => response));
  }
  saveDidConnect(did) {
    return this.http.put(`${environment.api.server}/did-numbers/assign`, did);
  }
  saveDidConnectAccount(did) {
    return this.http.put(`${environment.api.server}/did-numbers/assign-account`, did);
  }
  changeBarStatus(status, id) {
    return this.http.put(`${environment.api.server}/accounts/${id}/account-bar?isbar=${(status)? true : false}`, '');
  }
  deleteDid(id) {
    return this.http.delete(`${environment.api.server}/did-numbers/${id}`);
  }
}
