import {HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from 'environments/environment';

@Injectable()
export class TariffService {

    constructor(private http: HttpClient) {}

    getTariffByMinuts(accountid) {
        return this.http.get(`${environment.api.server}/accounts/${accountid}/minute-plans`);
    }
    getTariffByRates(accountid) {
        return this.http.get(`${environment.api.server}/accounts/${accountid}/rates`);
    }
}
