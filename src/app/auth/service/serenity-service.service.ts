import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SerenityServiceService {
    readonly url = environment.api.server + '/nodes';

    constructor(private http: HttpClient,
                private router: Router) {
    }

    createNode(_ipAddress, _name, _nodeType: number, _password, _privateKey, _url, _username): Observable<any> {
        const params = {
            ipAddress: _ipAddress,
            name: _name,
            nodeType: _nodeType,
            password: _password,
            privateKey: _privateKey,
            url: _url,
            username: _username
        };
        return this.http.post(this.url, params);
    }

    checkConnection(_ipAddress, _name, _nodeType: number, _password, _privateKey, _url, _username): Observable<any> {
        const params = {
            ipAddress: _ipAddress,
            name: _name,
            nodeType: _nodeType,
            password: _password,
            privateKey: _privateKey,
            url: _url,
            username: _username
        };
        return this.http.post(environment.api.server + '/nodes/check-connect', params);
    }

    updateNode(dataForUpdate: any): Observable<any> {
        const params = {
            id: dataForUpdate[0].id,
            ipAddress: dataForUpdate[0].ipAddress,
            name: dataForUpdate[0].name,
            nodeType: dataForUpdate[0].nodeType,
            password: dataForUpdate[0].password,
            privateKey: dataForUpdate[0].privateKey,
            url: dataForUpdate[0].url,
            username: dataForUpdate[0].username
        };
        return this.http.post(this.url, params);
    }

    searchNode(_id, _ipAddress, _name, _nodeType: number, _password, _privateKey, _url, _username): Observable<any> {
        const param = {
            id: _id,
            ipAddress: _ipAddress,
            name: _name,
            nodeType: _nodeType,
            password: _password,
            privateKey: _privateKey,
            url: _url,
            username: _username
        };
        console.log(param);
        // @ts-ignore
        return this.http.get(this.url, {params: param});
    }
}
