import { TestBed } from '@angular/core/testing';

import { SerenityServiceService } from './serenity-service.service';

describe('SerenityServiceService', () => {
  let service: SerenityServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SerenityServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
