import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerSearchService {

  constructor(private http: HttpClient) { }

  getCustomerSearch(post) {
    const userInfo:any = JSON.parse(window.localStorage.getItem('userInfo'));
    const keyParam = {
      "customerId": userInfo.username === 'admin' ? 0 : userInfo.customerId,
      'fromConnectTime': Date.parse(post.fromConnectTime),
      'endConnectTime': Date.parse(post.endConnectTime)
    };
    return this.http.post(`${environment.api.server}/customer-cdr/search`, keyParam).pipe(map((response: any) => response));
  }
  
}
