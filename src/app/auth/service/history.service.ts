import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class HistoryService {
    readonly url = environment.api.server + '/nodes';

    constructor(private http: HttpClient) {
    }

    getHistory(did) {
        return this.http.get(`${environment.api.server}/sms/${did}`).pipe(map((response: any) => response));
    }
}
