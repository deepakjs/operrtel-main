import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class SmsSearchService {
    readonly url = environment.api.server + '/nodes';

    constructor(private http: HttpClient) {
    }

    getSMSList(from, to) {
        return this.http.get(`${environment.api.server}/sms?from=${from}&to=${to}`).pipe(map((response: any) => response));
    }

    getAccounts(id) {
        return this.http.get(`${environment.api.server}/accounts?username=${id}`).pipe(map((response: any) => response));
    }

    getSmsFrom(id) {
        return this.http.get(`${environment.api.server}/sms/${id}`).pipe(map((response: any) => response));
    }

    saveSms(saveObj) {
        return this.http.post(`${environment.api.server}/sms/`, saveObj).pipe(map((response: any) => response));
    }

    getSmsFromNumber() {
        const number = window.localStorage.getItem('userMobileInfo');
        return this.http.post(`${environment.api.server}/subscribers/search`, {regNumber: number})
    }
}
