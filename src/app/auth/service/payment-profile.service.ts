import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable()
export class PaymentProfileService {
  static URL = environment.api.server + environment.api.billing.paymentProfile;
  static PAYMENT_URL = environment.api.server + environment.api.billing.payment;
  static PAYMENT_CARD_URL = environment.payment_gateway.server + '/api/v1';
  static PAYMENT_GATEWAY_APP_KEY = environment.payment_gateway.app_key;

  constructor(private http: HttpClient) {
  }

  getPaymentProfile(ownerType: any, ownerId: number): Observable<any> {
    return this.http.get(`${PaymentProfileService.URL}/${ownerType}/${ownerId}`);
  }

  getCreditCards(customerId: number): Observable<any> {
    return this.http.get(`${PaymentProfileService.URL}/card/CUSTOMER/${customerId}/list`);
  }
  getCreditCardsByType(ownerType: any, ownerId: number): Observable<any> {
    return this.http.get(`${PaymentProfileService.URL}/card/${ownerType}/${ownerId}/list`);
  }

  addPaymentMethod(paymentEncryptRequest: any): Observable<any> {
    return this.http.post<any>(`${PaymentProfileService.URL}/card`, paymentEncryptRequest);
  }

  deletePaymentMethod(customerProfileId: any, actionType: any): Observable<any> {
    return this.http.delete<any>(`${PaymentProfileService.URL}/card/${customerProfileId}/${actionType}`);
  }

  deleteProfile(profileId: any): Observable<any> {
    return this.http.delete<any>(`${PaymentProfileService.URL}/${profileId}`);
  }

  createPaymentProfile(payload: any): Observable<any> {
    return this.http.post<any>(`${PaymentProfileService.URL}`, payload);
  }

  updateCustomerPaymentProfile(id: number, payload): Observable<any> {
    return this.http.put<any>(`${PaymentProfileService.URL}/${id}`, payload);
  }

  testPayment(customerId: number, token: string): Observable<any> {
    return this.http.get(`${PaymentProfileService.PAYMENT_URL}/demo/2/${customerId}/${token}`);
  }

  getCreditCard(ownerType: any, ownerId: number, cardType: any): Observable<any> {
    return this.http.get(`${PaymentProfileService.URL}/card/${ownerType}/${ownerId}/${cardType}`);
  }

  deletePaymentProfileMethod(profileId: any, paymentProfileId: any, cardType: any): Observable<any> {
    return this.http.delete<any>(`${PaymentProfileService.URL}/card/${profileId}/${paymentProfileId}/${cardType}`);
  }

  applyChargeAmount(transferRequest) {
    return this.http.post(PaymentProfileService.URL + '/charge/amount', transferRequest);
  }

  valicateCardInfo(cardInfo) {
    return this.http.post(PaymentProfileService.URL + '/card-number/validate', cardInfo);
  }

  getBankName(rountingNumber) {
    return this.http.get(PaymentProfileService.URL + '/bank-name/' + rountingNumber);
  }

  getCardsByOwnerTypeAndId(ownerType: any, ownerId: number): Observable<any> {
    return this.http.get(`${PaymentProfileService.URL}/card/${ownerType}/${ownerId}/list`);
  }

  getPaymentKeyStore(): Observable<any> {
    return this.http.get<any>(`${PaymentProfileService.PAYMENT_CARD_URL}/payment-profile/get-key-store`);
  }

  addCardPaymentProfile(payload: any, storeKey: string): Observable<any> {
    return this.http.post<any>(`${PaymentProfileService.PAYMENT_CARD_URL}/payment-profile/add`, payload, {headers: {'X-Session-Id':storeKey,'X-App-Key':PaymentProfileService.PAYMENT_GATEWAY_APP_KEY }});
  }
  addPaymentDataToCustomer(data, customer) {
    // return this.http.post<any>(`${environment.api.server}/customers/${customer.customerId}/payment-profile`, data);
    return this.http.post<any>(`${environment.api.server}/customers/28599/payment-profile`, data);
  }
  getAllCreditCards(customerId: number): Observable<any> {
    const queryParams = new HttpParams().set('user_app_id', customerId.toString());
    console.log(queryParams);
    return this.http.get(`${PaymentProfileService.PAYMENT_CARD_URL}/payment-profile/list`, {params: queryParams, headers: {'X-App-Key':PaymentProfileService.PAYMENT_GATEWAY_APP_KEY}});
  }

  deletePaymentCard(id: any, customerId: any): Observable<any> {
    return this.http.delete<any>(`${PaymentProfileService.PAYMENT_CARD_URL}/payment-profile/card/${customerId}/${id}`, {headers: {'X-App-Key':PaymentProfileService.PAYMENT_GATEWAY_APP_KEY}});
  }

  makeDefaultCard(id: any, customerId: any): Observable<any> {
    const payload = {};
    return this.http.post<any>(`${PaymentProfileService.PAYMENT_CARD_URL}/payment-profile/card/${customerId}/${id}`, payload, {headers: {'X-App-Key':PaymentProfileService.PAYMENT_GATEWAY_APP_KEY }});
  }
}

// /card/{ownerType}/{ownerId}/list
// /card/{profileId}/{paymentProfileId}/{cardType}
