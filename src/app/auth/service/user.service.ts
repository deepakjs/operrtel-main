import {HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '@app/_model/user.model';
import { environment } from 'environments/environment';

@Injectable()
export class UserService {

    constructor(private http: HttpClient) {}

    getUserList(): Observable<User[]> {
        return this.http.get<User[]>(`${environment.api.server}/users`).pipe(map((response: any) => response));
    }

    saveUser(user): Observable<any> {
        return this.http.post(`${environment.api.server}/users`, user);
    }
    updateUser(user) {
        return this.http.put(`${environment.api.server}/users`, user);
    }
    deleteUser(id) {
        return this.http.delete(`${environment.api.server}/users/${id}` );
    }

    getUser(username) {
        return this.http.get(`${environment.api.server}/users/?username=${username}`);
    }
}
