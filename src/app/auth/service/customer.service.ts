import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient) { }

  getCustomers() {
    return this.http.get(`${environment.api.server}/customers`).pipe(map((response: any) => response));
  }
  getCustomer(customer) {
    return this.http.get(`${environment.api.server}/customers?firstName=${customer}`);
  }
  saveCustomer(customer) {
    return this.http.post(`${environment.api.server}/customers`, customer);
  }
  updateCustomer(customer) {
      return this.http.put(`${environment.api.server}/customers`, customer);
  }
  deleteCustomer(id) {
      return this.http.delete(`${environment.api.server}/customers/${id}` );
  }
}
