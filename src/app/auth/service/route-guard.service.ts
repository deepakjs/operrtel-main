import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from '@app/auth/service/auth.service';


@Injectable({
  providedIn: 'root'
})
export class RouteGuardService implements CanActivate {
    constructor(private authService: AuthService,
                private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const status = this.authService.isUserLoggedIn();
        if (status) {
            return true;
        }
        this.router.navigate(['login']);
        return false;
    }

}
