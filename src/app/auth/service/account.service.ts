import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  constructor(private http: HttpClient) { }
  getAccounts(id) {
    return this.http.get(`${environment.api.server}/accounts/customer-account?customerId=${id}`).pipe(map((response: any) => response));
  }
  changePassword(data, user) {
    return this.http.put(`${environment.api.server}/accounts/${user}/change-password`, data).pipe(map((response: any) => response));
  }
  savePayment(data, user) {
    return this.http.put(`${environment.api.server}/accounts`, {...data, username: user}).pipe(map((response: any) => response));
  }
  getAccountStatus(id) {
      return this.http.get(`${environment.api.server}/accounts/${id}/account-status`).pipe(map((response: any) => response));
  }
  getAccountFromSearch(account) {
    return this.http.get(`${environment.api.server}/accounts?username=${account}`).pipe(map((response: any) => response));
  }
  saveAccount(account) {
    account.balance = 0;
    account.billingPlan = 6;
    account.blocked = 0;
    account.dndEnabled = false;
    // account.email = "string";
    // account.firstName = "string";
    account.followmeEnabled = true;
    account.iaccount = 0;
    // account.lastName = "string";
    // account.phone = "string";
    account.preferredCodec = 8;
    // account.maxSessions = 4;
    account.routingGroup = 7;
    account.translationRule = "s/^[+]//;s/^011//;s/^(.{10})$/1\\1/";
    account.usePreferredCodecOnly = true;
    account.voipLogin = account.username;

    return this.http.post(`${environment.api.server}/accounts`, account);
  }
  changeBarStatus(status, id) {
    return this.http.put(`${environment.api.server}/accounts/${id}/account-bar?isBar=${status}`, '');
  }
  getAccountSearch(customerId) {
    return this.http.get(`${environment.api.server}/accounts/customer-account?customerId=${customerId}`);
  }
}
