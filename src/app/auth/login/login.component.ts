import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {AppUrl} from '@app/_config/constant/app-url.enum';
import {LoginType} from '@app/_config/constant/login-type.enum';
import {Router} from '@angular/router';
import {AuthService} from '../service/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm = this.fb.group({
        username: ['admin', Validators.required],
        password: ['Cleanair906', Validators.required]
    });
    phoneLogin = this.fb.group({
        phoneNumber: ['', Validators.required],
    });
    phoneLoginWithOTP = this.fb.group({
        username: ['', Validators.required],
        password: ['', Validators.required],
    });
    otpSent = false;
    message = '';
    failedMessage;

    showContractDialog = false;

    constructor(private fb: FormBuilder,
                private authService: AuthService,
                private router: Router) {
    }

    ngOnInit() {
        // this.translate.use(this.translate.currentLang ? this.translate.currentLang : 'en');
        this.onSignUp();
    }

    // defaultNavigate() {
    //     switch (this.authService.getLoginType()) {
    //         case LoginType.BILLER_ADMIN:
    //         case LoginType.BILLER_USER:
    //             this.router.navigate([AppUrl.BILLER]);
    //             break;
    //         case LoginType.BASE_ADMIN:
    //         case LoginType.BASE_USER:
    //             this.router.navigate([AppUrl.MAS]);
    //             break;
    //     }
    // }

    onPhoneLogin() {
        if(this.phoneLogin.valid) {
            this.authService.phoneLogin(this.phoneLogin.value).subscribe(success => {
                this.otpSent = true;
                this.phoneLoginWithOTP.controls['phoneNumber'].setValue(this.phoneLogin.value.phoneNumber);
            },
            error => {
                switch (error.status) {
                    case 0:
                        this.router.navigate([AppUrl.HOME]);
                        // this.message = 'Server Connection Error';
                        break;
                    case 404:
                        this.message = 'Incorrect Username/Password';
                        break;
                    case 500:
                        this.message = 'Internal Server Error';
                        break;
                    case 401:
                        this.failedMessage = 'Incorrect username/password. Please try again!';
                        break;
                    default:
                        this.message = error.error || error.message;
                }
            });
        }
    }
    onPhoneLoginWithOTP() {
        if(this.phoneLoginWithOTP.valid) {
            this.authService.phoneLoginOTP(this.phoneLoginWithOTP.value).subscribe((success: any) => {
                if(success.status === 'true') {
                    this.router.navigate([AppUrl.HOME]);
                } else {
                    this.message = success.message;
                }
                
            },
            error => {
                console.log(error);
                switch (error.status) {
                    case 0:
                        this.router.navigate([AppUrl.HOME]);
                        // this.message = 'Server Connection Error';
                        break;
                    case 404:
                        this.message = 'Incorrect Username/Password';
                        break;
                    case 500:
                        this.message = 'Internal Server Error';
                        break;
                    case 401:
                        this.failedMessage = 'Incorrect username/password. Please try again!';
                        break;
                    default:
                        this.message = error.error || error.message;
                }
            });
        }
    }
    onSignUp() {
        this.failedMessage = null;
        if (this.loginForm.valid) {
            this.authService.login(this.loginForm.value).subscribe(
                success => {
                    // this.defaultNavigate();
                    // this.router.navigate([AppUrl.HOME]);
                },
                error => {
                    console.log(error);
                    switch (error.status) {
                        case 0:
                            this.router.navigate([AppUrl.HOME]);
                            // this.message = 'Server Connection Error';
                            break;
                        case 404:
                            this.message = 'Incorrect Username/Password';
                            break;
                        case 500:
                            this.message = 'Internal Server Error';
                            break;
                        case 401:
                            this.failedMessage = 'Incorrect username/password. Please try again!';
                            break;
                        default:
                            this.message = error.error || error.message;
                    }
                });
        } else {
            this.message = '';
        }
    }

    openContractDialog() {
        this.showContractDialog = true;
    }

    onContractDialogDisplayChanged(showContract) {
        this.showContractDialog = showContract;
    }
}
